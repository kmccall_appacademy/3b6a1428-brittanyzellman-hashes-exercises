require 'byebug'
# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  str_hash = Hash.new
  str.split(" ").each do |word|
    str_hash[word] = word.length
  end
  str_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |_, v| v }[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  word_count = Hash.new(0)
  word.chars.each do |ch|
    word_count[ch] += 1
  end
  word_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  unique = Hash.new(0)
  arr.each do |keys|
    unique[keys] = 0
  end
  unique.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  evens_odds = {even: 0, odd: 0}
  numbers.each do |num|
    if num.even?
      evens_odds[:even] += 1
    else
      evens_odds[:odd] += 1
    end
  end
  evens_odds
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = {a: 0, e: 0, i: 0, o: 0, u: 0}
  string.each_char do |ch|
    if vowels.has_key?(ch.to_sym)
      vowels[ch.to_sym] += 1
    end
  end
  vowels_arr = vowels.sort_by { |_, v| v }
  vowels_arr.each do |pair|
    if pair[-1] == vowels_arr.last[-1]
      return pair[0].to_s
    end
  end
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students = students.select { |_, v| v >= 7 }
  arr_names = students.keys
  combinations(arr_names)
end

def combinations(arr)
  result = []
  i = 0
  while i < arr.length - 1
    j = i + 1
    while j < arr.length
      result << [arr[i], arr[j]]
      j += 1
    end
    i += 1
  end
  result
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  animals = Hash.new(0)
  specimens.each do |k, _|
    animals[k] += 1
  end
  animals = animals.sort_by { |_, v| v }.to_h
  population_sizes = animals.values
  pop_avg = population_sizes[0] / population_sizes[-1]
  (animals.length**2) / pop_avg
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  sign = character_count(normal_sign)
  vandal = character_count(vandalized_sign)
  vandal.each do |k, v|
    if sign.key?(k)
      return false if v > sign[k]
    end
  end
  true
end

def character_count(str)
  hsh = Hash.new(0)
  str.delete(",.;?!\' ")
  str.downcase!
  str.each_char do |ch|
    hsh[ch] += 1
  end
  hsh
end
